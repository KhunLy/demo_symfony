<?php

namespace App\Controller;

use App\Entity\Member;
use App\Form\Member\MemberCompanyType;
use App\Form\Member\MemberType;
use App\Repository\MemberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MemberController extends AbstractController
{
    /**
     * @Route(name="member_read", path="/members")
     */
    public function read(MemberRepository $mr)
    {
        $members = $mr->findAll();
        return $this->render('members/read.html.twig', [
           'members' => $members
        ]);
    }

    /**
     * @Route(name="member_create", path="/members/create")
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
        $member = new Member();
        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em->persist($member);
            $em->flush();
            return $this->redirectToRoute('member_read');
        }
        return $this->render('members/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(name="member_company_create", path="/members/company/create")
     */
    public function createMemberAndCompany(EntityManagerInterface $em, Request $request)
    {
        $member = new Member();
        $form = $this->createForm(MemberCompanyType::class, $member);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em->persist($member);
            //$em->persist($member->getCompany());
            $em->flush();
            return $this->redirectToRoute('member_read');
        }
        return $this->render('members/createWithCompany.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
