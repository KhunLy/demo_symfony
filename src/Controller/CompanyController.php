<?php


namespace App\Controller;


use App\Entity\Company;
use App\Form\Company\CompanyType;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    // CRUD

    /**
     * @param EntityManagerInterface $em
     * @param CompanyRepository $cr
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(path="/companies", name="company_read")
     */
    public function read(EntityManagerInterface $em, CompanyRepository $cr)
    {
//        $cr = $em->getRepository(Company::class);

        $companies = $cr->findAll();

        return $this->render("company/read.html.twig", [
            "companies" => $companies
        ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(path="/companies/create", name="company_create")
     */
    public function create(EntityManagerInterface $em, Request $request)
    {
        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);
        // Valide et ça injecte les données dans $company
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $em->persist($company);
                // Sauvegarde les données en db
                $em->flush();
                $this->addFlash('success', 'la compagnie a été ajoutée');
                return $this->redirectToRoute('company_read');
            }
            else{
                $this->addFlash('error', 'Les infos du formulaire sont incorrectes');
            }
        }

//        dump($company);

        return $this->render("company/create.html.twig", [
            "form" => $form->createView(), "company" => $company
        ]);
    }

    /**
     * @Route(name="company_update", path="/companies/update/{id}", requirements={"id"="\d+"})
     */
    public function update($id, CompanyRepository $cr, EntityManagerInterface $em, Request $request)
    {
        $company = $cr->find($id);
        $form = $this->createForm(CompanyType::class, $company);
        // Valide et ça injecte les données dans $company
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Sauvegarde les données en db
            $em->flush();

            return $this->redirectToRoute('company_read');
        }

//        dump($company);

        return $this->render("company/update.html.twig", [
            "form" => $form->createView(), "company" => $company
        ]);
    }

    /**
     * @Route(name="company_delete", path="/compagnies/delete/{id}", requirements={"id": "\d+"})
     */
    public function delete($id, CompanyRepository $cr, EntityManagerInterface $em)
    {
        // get the company to delete
        $companyToDelete =  $cr->find($id);
//        $companyToDelete = $cr->findOneBy(['id' => $id]);
        // remove the company from the entity manager
        $em->remove($companyToDelete);
        // save data in sgbd
        $em->flush();
        // ajouter un message "flash"
        $this->addFlash(
            'success',
            'La compagnie ' . $companyToDelete->getName() . ' a bien été supprimée'
        );
        return $this->redirectToRoute('company_read');
    }
}