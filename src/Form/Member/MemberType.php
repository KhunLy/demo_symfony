<?php

namespace App\Form\Member;

use App\Entity\Company;
use App\Entity\Member;
use App\Repository\CompanyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastName', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        $builder->add('firstName', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        $builder->add('company', EntityType::class, [
            'class' => Company::class,
            'choice_label' => 'name',
//            'multiple' => true,
//            'expanded' => true,
            'attr' => [
                'class' => 'maClasse'
            ],
            'query_builder' => function(CompanyRepository $cr) {
                return $cr->findActiveCompanies();
            }
        ]);
        $builder->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
