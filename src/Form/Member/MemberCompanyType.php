<?php

namespace App\Form\Member;

use App\Entity\Company;
use App\Entity\Member;
use App\Form\Company\CompanyType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MemberCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastName', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        $builder->add('firstName', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        $builder->add('company', CompanyType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
