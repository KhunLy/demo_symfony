<?php


namespace App\Form\Company;


use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class CompanyType extends AbstractType
{
    /*
     * Cette fonction vous permet de constuire le formulaire
     * => Créer les différents champs et les règles de validation
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('name', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        $builder->add('nbEmployees', IntegerType::class, [
            'constraints' => [
                new NotBlank(), new GreaterThanOrEqual(['value' => 0])
            ]
        ]);
        $builder->add('active', CheckboxType::class, [
            'required' => false,
            'constraints' => []
        ]);

        $builder->add('EnormeSUBMITBUTTON', SubmitType::class, [
            'label' => 'Licorne',
            'attr' => ['class' => 'button btn btn-michel']
        ]);
    }

    /*
     * D'aider Symfony à comprendre ce que vous voulez faire
     * A FAIRE EN PREMIER AFIN DE POUVOIR MOINS TRAVAILLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * $resolver->setDefault("data_class", ...)
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault("data_class", Company::class);
    }
}